/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.knightstale.implementations;

import com.mycompany.knightstale.data.QuestRisk;
import com.mycompany.knightstale.enums.Risk;
import com.mycompany.knightstale.interfaces.Knight;
import com.mycompany.knightstale.interfaces.KnightIF;
import com.mycompany.knightstale.interfaces.Quest;
import com.mycompany.knightstale.enums.KnightActivity;

/**
 *
 * @author Márta
 */
public class Szelmalmok implements Quest{

    @Override
    public String getQuestName() {
        return "Szélmalmok";
    }

    @Override
    public String getQuestDescription() {
        return "Látod,  barátom, a szerencse még jobban kedvez ügyednek, mint ahogyan "
                + "valaha is álmodni merted volna! Látod-e ott az óriások csapatát? "
                + "Küzdj meg velük, és foszd meg őket életüktől. Ami zsákmányra így "
                + "szert teszel, azzal megveted gazdagságod alapját, és bizonyára üdvös "
                + "dolgot mívelsz, ha ezt a gonosz fajzatot kiirtod a föld színéről.";
    }

    @Override
    public QuestRisk getRiskOfQuest() {
        return new QuestRisk(Risk.none, Risk.high);
    }

    @Override
    public int getQuestFame() {
        return 0;
    }

    @Override
    public boolean doQuest(KnightIF knight) {
        KnightActivity knightActivity=knight.actInQuest(this);
        if(knightActivity==KnightActivity.attackUnexpectedly||
                knightActivity==KnightActivity.defendWithShield||
                knightActivity==KnightActivity.strikeWithSword)knight.addHealth(-10);
        return true;
    }
    
}
