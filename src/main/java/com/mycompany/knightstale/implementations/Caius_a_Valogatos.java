/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.knightstale.implementations;

import com.mycompany.knightstale.enums.KnightActivity;
import com.mycompany.knightstale.interfaces.HighLady;
import com.mycompany.knightstale.interfaces.Knight;
import com.mycompany.knightstale.interfaces.Quest;
import java.util.HashSet;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Krisz
 */
@Component()
@Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
public class Caius_a_Valogatos extends Knight{
    
    private Set<String> questsKnown;
    private int latestChoice = 1;

    @Autowired
    public Caius_a_Valogatos(@Qualifier("randomLady") HighLady myLady) {
        super(myLady);
        questsKnown = new HashSet<>();
    }

    @Override
    public boolean takesQuest(Quest quest) {
        boolean haveFound = questsKnown.contains(quest.getQuestName());
        questsKnown.add(quest.getQuestName());
        return (!haveFound);
    }

    @Override
    public KnightActivity actInQuest(Quest quest) {
        KnightActivity ka;
        switch(latestChoice){
            case 8:
                ka = KnightActivity.attackUnexpectedly;
                break;
            case 7:
                ka = KnightActivity.chantALoveSong;
                break;
            case 6:
                ka = KnightActivity.defendWithShield;
                break;
            case 5:
                ka = KnightActivity.makeSpeechBeforeFight;
                break;
            case 4:
                ka = KnightActivity.praiseMyLady;
                break;
            case 3:
                ka = KnightActivity.writeALovePoem;
                break;
            case 2:
                ka = KnightActivity.runAway;
                break;
            case 1:
            default:
                ka = KnightActivity.strikeWithSword;
        }
        latestChoice++;
        if(latestChoice>8) latestChoice = 1;
        return ka;
    }
    
}
