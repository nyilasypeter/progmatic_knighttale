package com.mycompany.knightstale.implementations;

import com.mycompany.knightstale.data.QuestRisk;
import com.mycompany.knightstale.enums.KnightActivity;
import com.mycompany.knightstale.enums.Risk;
import com.mycompany.knightstale.interfaces.Knight;
import com.mycompany.knightstale.interfaces.KnightIF;
import com.mycompany.knightstale.interfaces.Quest;

/**
 *
 * @author Simon, Gergely
 */
public class Bagolyvar implements Quest{

    @Override
    public String getQuestName() {
        return "Bagolyvár";
    }

    @Override
    public String getQuestDescription() {
        return "A baglyok nem azok amiknek látszanak.";
    }

    @Override
    public QuestRisk getRiskOfQuest() {
        return new QuestRisk(Risk.unknown, Risk.unknown);
    }

    @Override
    public int getQuestFame() {
        return 100;
    }

    @Override
    public boolean doQuest(KnightIF knight) {
        if (knight.actInQuest(this).equals(KnightActivity.praiseMyLady)) {
            knight.addHealth(50);
            return true;
        }
        if (knight.actInQuest(this).equals(KnightActivity.runAway) && knight.getHealth() <=50) {
            return true;
        }
        if (knight.actInQuest(this).equals(KnightActivity.attackUnexpectedly)) {
            knight.addHealth(-50);
            return true;
        }
        else {
            return false;
        }
    }
    
}
