/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.knightstale.implementations;

import com.mycompany.knightstale.data.QuestRisk;
import com.mycompany.knightstale.enums.KnightActivity;
import com.mycompany.knightstale.enums.Risk;
import com.mycompany.knightstale.interfaces.Knight;
import com.mycompany.knightstale.interfaces.KnightIF;
import com.mycompany.knightstale.interfaces.Quest;

/**
 *
 * @author Krisz
 */
public class Dana_Tornya implements Quest{

    @Override
    public String getQuestName() {
        return "Dana Tornya";
    }

    @Override
    public String getQuestDescription() {
        String story;
        story = "Dana istennőt rabul ejtette a rút Balor, és várának legmagasabb tornyába záratta. "
                + "Samhain éjjelén Balor szolgái nem mernek a várfalon őrt állni, mert rettegnek a "
                + "holtak seregétől. Ha elhiteted a katonákkal, hogy a holtak támadásra készülnek, "
                + "kiszabadíthatod az istennőt. Egy szívből szóló költeménnyel is felhívhatod "
                + "magadra a fogoly figyelmét, ám légy résen: ha nagy zajt csapsz, vagy vakmerő "
                + "tettre vetemedsz, halálos nyílzáporral kell szembe nézned.";
        return story;
    }

    @Override
    public QuestRisk getRiskOfQuest() {
        return new QuestRisk(Risk.low,Risk.high);
    }

    @Override
    public int getQuestFame() {
        return 175;
    }

    @Override
    public boolean doQuest(KnightIF knight) {
        KnightActivity actInQuest;
        
        for(int i=0;i<4;i++){
            actInQuest = knight.actInQuest(this);
            switch(actInQuest){
                case makeSpeechBeforeFight:  
                    /*System.out.println("Szónoklatod hatására félelem költözik Balor szolgáinak szívébe, "
                            + "és vastag ajtók mögé zárkóznak. Ellenállás nélkül teszed meg az utad a "
                            + "toronyig, hogy kiszabadítsd az istennőt.");*/
                    return true;
                case defendWithShield:
                    /*System.out.println("Bár az őrők nem merészkednek ki a várfalra, azért hosszú "
                            + "íjaikat bevetik ellened. Pajzsodat fedezékül használva közelíted "
                            + "meg a toronyot.");*/
                    int dmg = (int)(Math.random()*10)+1;
                    knight.addHealth(-dmg);
                    return knight.getHealth()>0;
                case writeALovePoem: 
                case chantALoveSong:
                case praiseMyLady:
                    /*System.out.println("Egy trubadúr az ellenséges falak alatt is trubadúr... "
                            + "Szónoklatoddal magadra vonod az íjászok figyelmét, amíg Dana "
                            + "lopva távozik fogságából.");*/
                    dmg = (int)(Math.random()*15)+1;
                    knight.addHealth(-dmg);
                    return knight.getHealth()>0;
                case attackUnexpectedly:
                case strikeWithSword:
                    /*System.out.println("Egymagad intézel támadást az erődítmény ellen. "
                            + "Még a várárokig sem jutsz el, és nyílvesszők százai kezdenek "
                            + "záporozni. Kétségbeesetten menedék után nézel.");*/
                    dmg = (int)(Math.random()*20)+1;
                    knight.addHealth(-dmg);
                    if(knight.getHealth()<=0) return false;
                    else break;
                case runAway:
                default:
                    /*System.out.println("Jobbnak látod kereket oldani.");*/
                    return false;
            }
        }
        
        /*System.out.println("Nincs több ötleted, hogyan menthetnéd ki a szerencsétlenül "
                + "járt Danát. Szégyenteljes visszavonulót fújsz.");*/
        return false;
    }
    
}
