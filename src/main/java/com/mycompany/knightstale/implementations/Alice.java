package com.mycompany.knightstale.implementations;

import com.mycompany.knightstale.interfaces.HighLady;

/**
 *
 * @author Attila
 */
public class Alice extends HighLady {

    @Override
    public int fameExpectedFromKnight() {
        return 25000;
    }

}
