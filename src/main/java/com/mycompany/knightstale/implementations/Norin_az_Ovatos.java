/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.knightstale.implementations;

import com.mycompany.knightstale.enums.KnightActivity;
import com.mycompany.knightstale.enums.Risk;
import com.mycompany.knightstale.interfaces.HighLady;
import com.mycompany.knightstale.interfaces.Knight;
import com.mycompany.knightstale.interfaces.Quest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Krisz
 */
@Component()
@Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
public class Norin_az_Ovatos extends Knight{
    
    private int healthCathegory;

    @Autowired
    public Norin_az_Ovatos(@Qualifier("randomLady") HighLady myLady) {
        super(myLady);
    }
    
    public void getHealthCathegory(){
        if(getHealth()>=100) healthCathegory = 4;
        else if(getHealth()>=80) healthCathegory = 3;
        else if(getHealth()>=50) healthCathegory = 2;
        else healthCathegory = 1;
    }
    
    @Override
    public boolean takesQuest(Quest quest) {
        
        getHealthCathegory();
        
        Risk riskOfDeath = quest.getRiskOfQuest().getProbabilityOfDeath();
        Risk riskOfInjury = quest.getRiskOfQuest().getProbabililtyOfInjury();
        
        switch(healthCathegory){
            case 4:
                return !(riskOfDeath.equals(Risk.unknown));
            case 3:
                return !(riskOfDeath.equals(Risk.unknown) || riskOfDeath.equals(Risk.high)
                        || riskOfInjury.equals(Risk.unknown));
            case 2:
                return !(riskOfDeath.equals(Risk.unknown) || riskOfDeath.equals(Risk.high)
                        || (riskOfDeath.equals(Risk.low)) || riskOfInjury.equals(Risk.unknown)
                        || riskOfInjury.equals(Risk.high));
            case 1:
                return (riskOfDeath.equals(Risk.none) && riskOfInjury.equals(Risk.none));
        }
        return false;
    }

    @Override
    public KnightActivity actInQuest(Quest quest) {
        
        getHealthCathegory();
        
        int dieRoll = (int)(Math.random()*8)+1;
        KnightActivity ka = KnightActivity.runAway;
        
        switch(healthCathegory){
            case 4:
                switch(dieRoll){
                    case 8: return KnightActivity.attackUnexpectedly;
                    case 7: return KnightActivity.chantALoveSong;
                    case 6: return KnightActivity.defendWithShield;
                    case 5: return KnightActivity.makeSpeechBeforeFight;
                    case 4: return KnightActivity.praiseMyLady;
                    case 3: return KnightActivity.writeALovePoem;
                    case 2: return KnightActivity.runAway;
                    case 1: return KnightActivity.strikeWithSword;
                }
            case 3:
                switch(dieRoll){
                    case 8: return KnightActivity.strikeWithSword;
                    case 7: return KnightActivity.makeSpeechBeforeFight;
                    case 6: return KnightActivity.praiseMyLady;
                    case 5: return KnightActivity.writeALovePoem;
                    case 4: return KnightActivity.defendWithShield;
                    case 3:
                    case 2:
                    case 1: return KnightActivity.runAway;
                }
            case 2:
                switch(dieRoll){
                    case 8: return KnightActivity.strikeWithSword;
                    case 7: return KnightActivity.praiseMyLady;
                    case 6: return KnightActivity.defendWithShield;
                    case 5: return KnightActivity.writeALovePoem;
                    case 4:
                    case 3:
                    case 2:
                    case 1: return KnightActivity.runAway;
                }
            case 1:
                switch(dieRoll){
                    case 8: return KnightActivity.strikeWithSword;
                    case 7: return KnightActivity.praiseMyLady;
                    case 6:
                    case 5:
                    case 4:
                    case 3:
                    case 2:
                    case 1: return KnightActivity.runAway;
                }
        }
        return ka;
    }
    
}
