package com.mycompany.knightstale.implementations;

import com.mycompany.knightstale.data.QuestRisk;
import com.mycompany.knightstale.enums.KnightActivity;
import com.mycompany.knightstale.enums.Risk;
import com.mycompany.knightstale.interfaces.Knight;
import com.mycompany.knightstale.interfaces.KnightIF;
import com.mycompany.knightstale.interfaces.Quest;
import org.springframework.stereotype.Component;

/**
 *
 * @author Attila
 */
public class TheRussianMagician implements Quest {

    @Override
    public String getQuestName() {
        return "Az őrült Varázsló";
    }

    @Override
    public String getQuestDescription() {
        String story = "A küldetésed egy titokzatos ám kissé szeszélyes varázsló Mayakovsky"
                + "felkutatása, azzal a céllal, hogy az áhított hölgyed kedvébe"
                + "tegyél, ám a lehetlent készülsz megvalósítani, ugyanis csakis"
                + "azzal nyerheted el a hölgyeményt, ha teljesíted azt a kérését"
                + "miszerint halott testvérét kell a halálból visszahozzad."
                + "Találkozol is a varázslóval, majd egy félresikerült varázslat"
                + "miatt egy idegen  varázslényt sikerült megidézni ami azonnal megölte a varázslót."
                + "Csakis rajtad áll, hogy mit teszel a megidézett szörnnyel.";
        return story;
    }

    @Override
    public QuestRisk getRiskOfQuest() {
        return new QuestRisk(Risk.high, Risk.high);
    }

    @Override
    public int getQuestFame() {
        return 25000;
    }

    @Override
    public boolean doQuest(KnightIF knight) {
        System.out.println("RussianMagician doQuest " + knight.getName());
        int enemyHealth = 200;
        boolean hasShield = true;
        while (knight.getHealth() > 0 && enemyHealth > 0) {
            KnightActivity actInQuest = knight.actInQuest(this);
            System.out.println("RussianMagician doQuest in while loop: " + actInQuest);
            switch (actInQuest) {
                case makeSpeechBeforeFight:
                    knight.addHealth(-15);
                    enemyHealth -= 15;
                    break;
                case strikeWithSword:
                    knight.addHealth(-5);
                    enemyHealth -= 40;
                    break;
                case defendWithShield:
                    if (hasShield) {
                        hasShield = false;
                        knight.addHealth(20);

                    } else {
                        knight.addHealth(-40);
                    }
                    break;
                case attackUnexpectedly:
                    enemyHealth -= 40;
                    knight.addHealth(-21);
                    break;
                case runAway:
                    knight.addFame(-1000);
                    break;
                case writeALovePoem:
                    knight.addHealth(-100);
                    break;
                case chantALoveSong:
                    knight.addHealth(-100);
                    break;
                case praiseMyLady:
                    knight.addHealth(-100);
                    break;
                default:
                    return true;
            }
        }
        return knight.getHealth() > 0;
    }

}
