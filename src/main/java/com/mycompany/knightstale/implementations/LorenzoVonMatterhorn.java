package com.mycompany.knightstale.implementations;

import com.mycompany.knightstale.enums.KnightActivity;
import com.mycompany.knightstale.interfaces.HighLady;
import com.mycompany.knightstale.interfaces.Knight;
import com.mycompany.knightstale.interfaces.Quest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Attila
 */
@Component
@Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
public class LorenzoVonMatterhorn extends Knight {

    @Autowired
    public LorenzoVonMatterhorn(@Qualifier("randomLady") HighLady myLady) {
        super(myLady);
    }

    @Override
    public boolean takesQuest(Quest quest) {
        String questname = "Az őrült Varázsló";
        String q = quest.getQuestName();
        if (q.equals(questname)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public KnightActivity actInQuest(Quest quest) {
       // int random = (int) (Math.random() * 8) + 1;;
       int random = 2;
        switch (random) {
            case 1:
                return KnightActivity.makeSpeechBeforeFight;
            case 2:
                return KnightActivity.strikeWithSword;

            case 3:
                return KnightActivity.defendWithShield;

            case 4:
                return KnightActivity.attackUnexpectedly;

            case 5:
                return KnightActivity.runAway;

            case 6:
                return KnightActivity.writeALovePoem;

            case 7:
                return KnightActivity.chantALoveSong;
            case 8:
                return KnightActivity.praiseMyLady;

        }
        return KnightActivity.runAway;
    }

}
