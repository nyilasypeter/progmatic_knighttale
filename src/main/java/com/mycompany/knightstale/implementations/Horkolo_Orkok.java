package com.mycompany.knightstale.implementations;

import com.mycompany.knightstale.data.QuestRisk;
import com.mycompany.knightstale.enums.KnightActivity;
import com.mycompany.knightstale.enums.Risk;
import com.mycompany.knightstale.interfaces.Knight;
import com.mycompany.knightstale.interfaces.KnightIF;
import com.mycompany.knightstale.interfaces.Quest;
import org.springframework.stereotype.Component;

public class Horkolo_Orkok implements Quest{
    
    @Override
    public String getQuestName() {
        return "Horkoló orkok.";
    }

    @Override
    public String getQuestDescription() {
         return "Horkoló orkok description.";
    }

    @Override
    public QuestRisk getRiskOfQuest() {
        return new QuestRisk(Risk.high,Risk.unknown);
    }

    @Override
    public int getQuestFame() {
        return 250;
    }

    @Override
    public boolean doQuest(KnightIF knight) {
        KnightActivity actInQuest = knight.actInQuest(this);
        switch(actInQuest){
            case makeSpeechBeforeFight:  
            case writeALovePoem: 
            case chantALoveSong:
            case praiseMyLady:
                knight.addHealth(-knight.getHealth());
                return false;
            default:
                return true;
        }
    }
    
    
}
