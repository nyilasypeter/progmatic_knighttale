/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.knightstale.implementations;

import com.mycompany.knightstale.enums.KnightActivity;
import com.mycompany.knightstale.enums.Risk;
import com.mycompany.knightstale.interfaces.HighLady;
import com.mycompany.knightstale.interfaces.Knight;
import com.mycompany.knightstale.interfaces.Quest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Gyozo
 */
@Component
@Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
public class Finn_Thor extends Knight {
    
    int healthBeforeLastAction;

    @Autowired
    public Finn_Thor(@Qualifier("randomLady") HighLady myLady) {
        super(myLady);
    }

    @Override
    public boolean takesQuest(Quest quest) {
        healthBeforeLastAction = getHealth();
        return quest.getRiskOfQuest().getProbabililtyOfInjury() == Risk.unknown && quest.getRiskOfQuest().getProbabilityOfDeath() == Risk.unknown && getHealth() >= 20;
    }

    @Override
    public KnightActivity actInQuest(Quest quest) {
        if (getHealth() == healthBeforeLastAction) {
            return KnightActivity.defendWithShield;
        } else {
            healthBeforeLastAction = getHealth();
            return KnightActivity.runAway;
        }
    }
    
}
