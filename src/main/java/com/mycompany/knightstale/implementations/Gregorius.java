/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.knightstale.implementations;

import com.mycompany.knightstale.enums.KnightActivity;
import com.mycompany.knightstale.enums.Risk;
import com.mycompany.knightstale.interfaces.HighLady;
import com.mycompany.knightstale.interfaces.Knight;
import com.mycompany.knightstale.interfaces.Quest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Simon, Gergely
 */
@Component
@Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
public class Gregorius extends Knight{

    @Autowired
    public Gregorius(@Qualifier("randomLady") HighLady myLady) {
        super(myLady);
    }

    @Override
    public boolean takesQuest(Quest quest) {
        return quest.getRiskOfQuest().getProbabililtyOfInjury().equals(Risk.high) || 
                quest.getRiskOfQuest().getProbabililtyOfInjury().equals(Risk.unknown) ||
                quest.getRiskOfQuest().getProbabilityOfDeath().equals(Risk.high) ||
                quest.getRiskOfQuest().getProbabilityOfDeath().equals(Risk.unknown);
    }

    @Override
    public KnightActivity actInQuest(Quest quest) {
        if (quest.getQuestDescription().contains("hölgy")) {
            return KnightActivity.chantALoveSong;
        } else if (Gregorius.super.getHealth() >= 90) {
            return KnightActivity.makeSpeechBeforeFight;
        } else if (Gregorius.super.getHealth() >= 5) {
            return KnightActivity.strikeWithSword;
        } else {
            return KnightActivity.praiseMyLady;
            
            
            
            
        }
    }
    
}
