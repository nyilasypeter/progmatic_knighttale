package com.mycompany.knightstale.implementations;

import com.mycompany.knightstale.enums.KnightActivity;
import com.mycompany.knightstale.enums.Risk;
import com.mycompany.knightstale.interfaces.HighLady;
import com.mycompany.knightstale.interfaces.Knight;
import com.mycompany.knightstale.interfaces.Quest;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component()
@Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
public class Sir_Christian extends Knight{

    public Sir_Christian(HighLady myLady) {
        super(myLady);
    }

    @Override
    public boolean takesQuest(Quest quest) {
        if(quest.getRiskOfQuest().getProbabilityOfDeath().equals(Risk.high) ||
                quest.getRiskOfQuest().getProbabililtyOfInjury().equals(Risk.high)) return true;
        else return false;
    }

    @Override
    public KnightActivity actInQuest(Quest quest) {
        return KnightActivity.strikeWithSword;
    }
    
}
