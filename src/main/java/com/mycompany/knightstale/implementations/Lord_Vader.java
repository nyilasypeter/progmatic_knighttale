/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.knightstale.implementations;

import com.mycompany.knightstale.data.QuestRisk;
import com.mycompany.knightstale.enums.KnightActivity;
import com.mycompany.knightstale.enums.Risk;
import com.mycompany.knightstale.interfaces.Knight;
import com.mycompany.knightstale.interfaces.KnightIF;
import com.mycompany.knightstale.interfaces.Quest;

/**
 *
 * @author Balazs
 */
public class Lord_Vader implements Quest {

    @Override
    public String getQuestName() {
        return "Lord Vader";
    }

    @Override
    public String getQuestDescription() {
        return "Az erdőben lovagolva egyszer csak különös, még sosem hallott hangokra leszel figyelmes. Hirtelen a lóhátról leesve a földön találod magad, a lovad pedig megvadulva vágtat a sötét erdő felé. Pár pillanat múlva már nem is látod a sűrű bozótban, és egy hátborzongató hang kérdezi tőled: - Látta már ezt a droidot?";
    }

    @Override
    public QuestRisk getRiskOfQuest() {
        return new QuestRisk(Risk.high, Risk.high);
    }

    @Override
    public int getQuestFame() {
        return 250;
    }

    @Override
    public boolean doQuest(KnightIF knight) {
        KnightActivity actInQuest = knight.actInQuest(this);
        switch (actInQuest) {
            case chantALoveSong:
            case praiseMyLady:
            case writeALovePoem:
                knight.addHealth(-5);
                knight.addFame(10);
                return false;
            case defendWithShield:
                knight.addHealth(-50);
                return false;
            case runAway:
                knight.addHealth(-3);
                return true;
            default:
                knight.addHealth(-knight.getHealth());
                return false;
        }
    }

}
