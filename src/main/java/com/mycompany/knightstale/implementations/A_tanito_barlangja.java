/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.knightstale.implementations;

import com.mycompany.knightstale.data.QuestRisk;
import com.mycompany.knightstale.enums.KnightActivity;
import com.mycompany.knightstale.enums.Risk;
import com.mycompany.knightstale.interfaces.Knight;
import com.mycompany.knightstale.interfaces.KnightIF;
import com.mycompany.knightstale.interfaces.Quest;

/**
 *
 * @author Krisz
 */
public class A_tanito_barlangja implements Quest{

    @Override
    public String getQuestName() {
        return "A tanító barlangja";
    }

    @Override
    public String getQuestDescription() {
        return "Állítólag csak azt találod odabenn, amit beviszel magaddal... "
                + "El tudsz tölteni négy éjszakát sötétségben és magányban?";
    }

    @Override
    public QuestRisk getRiskOfQuest() {
        return new QuestRisk(Risk.none,Risk.none);
    }

    @Override
    public int getQuestFame() {
        return 75;
    }

    @Override
    public boolean doQuest(KnightIF knight) {
        int sanity = knight.getHealth(); 
                
        KnightActivity actInQuest;
        
        for(int i=0;i<4;i++){
            actInQuest = knight.actInQuest(this);
            switch(actInQuest){
                case makeSpeechBeforeFight:  
                    /*System.out.println("A homályban megmoccan egy árny. Felszólítod, hogy lépjen elő, "
                            + "és lovaghoz méltó módon mérjétek össze erőtöket. Szavaidra az árny szerte "
                            + "foszlik, és megerősödsz a hitedben.");*/
                    sanity += 10;
                    break;
                case writeALovePoem: 
                case chantALoveSong:
                case praiseMyLady:
                    /*System.out.println("A sötétségben felidézed hölgyed arcát, és boldogan "
                            + "dalra fakadsz. Felemelő érzés.");*/
                    sanity += 5;
                    break;
                case defendWithShield:
                    /*System.out.println("Halk neszre leszel figyelmes, amitől feláll a hátadon "
                            + "a szőr. Felkapod a pajzsod, hogy megnézd a hang forrását. Végül "
                            + "semmit nem találsz, és kimerülten térsz vissza a fekhelyedhez. ");*/
                    sanity -= 10;
                    break;
                case strikeWithSword:
                    /*System.out.println("Az esti imádságodat egy fekete páncélt viselő lovag "
                            + "szakítja félbe. Egy csapással leteríted az idegent, aki szellemként "
                            + "semmivé foszlik. Különös levertség vesz rajtad erőt.");*/
                    sanity -= 20;
                    break;
                case attackUnexpectedly:
                    /*System.out.println("Az éjszaka közepén arra ébredsz, hogy valaki a hátad "
                            + "mögött szuszog. Lassan megragadod a kardod, és váratlanul lecsapsz. "
                            + "Későn fedezed fel, hogy az alak kísértetiesen hasonlít szíved hölgyére. "
                            + "Bánat keírt a hatalmába.");*/
                    sanity -= 30;
                    break;
                case runAway:
                default:
                    /*System.out.println("Elméd kezd megháborodni a sötétségtől és a magánytól. "
                            + "Feladod a küzdelmet.");*/
                    return false;
            }
        }
        //System.out.println(sanity>0 ? "Kiálltad a próbát." : "A próbát elbuktad.");
        return sanity>0;
    }
    
}
