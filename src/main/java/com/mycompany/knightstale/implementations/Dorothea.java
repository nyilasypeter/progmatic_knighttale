package com.mycompany.knightstale.implementations;

import com.mycompany.knightstale.interfaces.HighLady;

/**
 *
 * @author Simon, Gergely
 */
public class Dorothea extends HighLady{

    @Override
    public int fameExpectedFromKnight() {
        return 850;
    }
    
}
