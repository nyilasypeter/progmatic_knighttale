/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.knightstale.implementations;

import com.mycompany.knightstale.interfaces.HighLady;

/**
 *
 * @author Márta
 */
public class Dulcinea extends HighLady{
    
    @Override
    public int fameExpectedFromKnight(){
        return 1000;
    }
    
}
