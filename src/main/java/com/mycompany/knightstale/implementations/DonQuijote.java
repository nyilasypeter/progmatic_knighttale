/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.knightstale.implementations;

import com.mycompany.knightstale.enums.KnightActivity;
import com.mycompany.knightstale.interfaces.HighLady;
import com.mycompany.knightstale.interfaces.Knight;
import com.mycompany.knightstale.interfaces.Quest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Márta
 */
@Component
@Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
public class DonQuijote extends Knight{

    @Autowired
    public DonQuijote(@Qualifier("randomLady") HighLady myLady) {
        super(myLady);
    }

    @Override
    public boolean takesQuest(Quest quest) {
        return true;
    }

    @Override
    public KnightActivity actInQuest(Quest quest) {
        System.out.println("Senki ne merészeljen tovább menni, míg ki nem jelenti, "
                + "hogy az egész kerek világon nincsen olyan hölgy, aki szépségben "
                + "la Mancha császárnőjével, a páratlan Dulcinea del Tobosóval versenyezhetne.");
        return KnightActivity.makeSpeechBeforeFight;
    }
    
}
