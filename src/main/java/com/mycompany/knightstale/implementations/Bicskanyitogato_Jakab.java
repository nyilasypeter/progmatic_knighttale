/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.knightstale.implementations;

import com.mycompany.knightstale.enums.KnightActivity;
import com.mycompany.knightstale.enums.Risk;
import com.mycompany.knightstale.interfaces.HighLady;
import com.mycompany.knightstale.interfaces.Knight;
import com.mycompany.knightstale.interfaces.Quest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Balazs
 */
@Component
@Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
public class Bicskanyitogato_Jakab extends Knight {

    @Autowired
    public Bicskanyitogato_Jakab(@Qualifier("randomLady") HighLady myLady) {
        super(myLady);
    }

    @Override
    public boolean takesQuest(Quest quest) {
        return quest.getRiskOfQuest().getProbabililtyOfInjury().equals(Risk.high)
                || quest.getRiskOfQuest().getProbabilityOfDeath().equals(Risk.high);
    }

    @Override
    public KnightActivity actInQuest(Quest quest) {
        int activityNUmber = (int) (Math.random() * 8);
        switch (activityNUmber) {
            case 0:
                return KnightActivity.attackUnexpectedly;
            case 1:
                return KnightActivity.chantALoveSong;
            case 2:
                return KnightActivity.defendWithShield;
            case 3:
                return KnightActivity.makeSpeechBeforeFight;
            case 4:
                return KnightActivity.praiseMyLady;
            case 5:
                return KnightActivity.runAway;
            case 6:
                return KnightActivity.strikeWithSword;
            default:
                return KnightActivity.writeALovePoem;
        }
    }
}
