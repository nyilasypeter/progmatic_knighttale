package com.mycompany.knightstale.implementations;

import com.mycompany.knightstale.interfaces.HighLady;
import org.springframework.stereotype.Component;

/**
 *
 * @author Krisz
 */
public class Tank_Aranka extends HighLady{

    @Override
    public int fameExpectedFromKnight() {
        return 666;
    }
    
}
