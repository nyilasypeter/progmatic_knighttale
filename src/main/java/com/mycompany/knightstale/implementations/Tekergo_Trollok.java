/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.knightstale.implementations;

import com.mycompany.knightstale.data.QuestRisk;
import com.mycompany.knightstale.enums.Risk;
import com.mycompany.knightstale.interfaces.Knight;
import com.mycompany.knightstale.interfaces.KnightIF;
import com.mycompany.knightstale.interfaces.Quest;

/**
 *
 * @author Gyozo
 */
public class Tekergo_Trollok implements Quest{
    
    int trollNumber;
    int trollsKilled;
    int currentTrollHealth;
    
    public Tekergo_Trollok(){
        int random = (int)(Math.random() * 6 - 2);
        this.trollNumber = Math.max(0, random);
        this.trollsKilled = 0;
    }

    @Override
    public String getQuestName() {
        return "Tekergő trollok";
    }

    @Override
    public String getQuestDescription() {
        return "Nem megbízható források szerint trollok tekeregnek egy közeli tölgyerdő területén. Szakképzett szörnyvadászt szeretnénk szerződtetni, aki szétnézne.";
    }

    @Override
    public QuestRisk getRiskOfQuest() {
        return new QuestRisk(Risk.unknown, Risk.unknown);
    }

    @Override
    public int getQuestFame() {
        return 50 + (trollsKilled * 100);
    }

    @Override
    public boolean doQuest(KnightIF knight) {
        while(trollNumber > 0){
            currentTrollHealth = 5;
            do{
                switch(knight.actInQuest(this)){
                    case defendWithShield:
                        currentTrollHealth -= knight.getFame() / 100;
                        if (currentTrollHealth > 0) {
                            knight.addHealth(Math.min(0, -4 + knight.getFame() / 100));
                            if (knight.getHealth() < 1) {
                                return false;
                            }
                        }
                        break;
                    case runAway:
                        return true;
                    case attackUnexpectedly:
                        if(trollsKilled == 0){
                            currentTrollHealth = 0;
                            break;
                        }
                    case strikeWithSword:
                        currentTrollHealth -= 2 + knight.getFame() / 100;
                    default:
                        if (currentTrollHealth > 0) {
                            knight.addHealth(Math.min(-1, -8 + knight.getFame() / 100));
                            if (knight.getHealth() < 1) {
                                return false;
                            }
                        }
                }
            }while(currentTrollHealth > 0);
            trollsKilled++;
            trollNumber--;
        }
        return true;
    }
    
}