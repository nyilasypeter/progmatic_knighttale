/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.knightstale;

import com.mycompany.knightstale.interfaces.Knight;
import com.mycompany.knightstale.interfaces.KnightIF;
import com.mycompany.knightstale.spring.Springconfig;
import com.mycompany.knightstale.interfaces.Quest;
import java.util.ArrayList;
import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author peti
 */
public class StoryBoard {

    private final int nrOfRounds = 100;
    private final int nrOfKnights = 10;

    List<KnightIF> knights;

    ApplicationContext context;

    public static void main(String[] args) {
        try {
            StoryBoard sb = new StoryBoard();
            sb.context = new AnnotationConfigApplicationContext(Springconfig.class);

            sb.makeKnightStory();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void makeKnightStory() {
        knights = getKnights(nrOfKnights);
        for (int i = 0; i < nrOfRounds; i++) {
            for (int j = 0; j < knights.size(); j++) {
                KnightIF knight = knights.get(j);
                Quest quest = getAQuest();
                if (knight.takesQuest(quest)) {
                    boolean result = quest.doQuest(knight);
                    if (result) {
                        knight.addFame(quest.getQuestFame());
                        System.out.println(String.format("A nagyszerű hős lovag: %s "
                                + "soha nem múló érdemeket szerzett "
                                + "e kaland során. Hírneve (őrzi a síron túl is) "
                                + "most már eléri a %d-t. Egészsége mindeközben: %d.",
                                knight.getName(),
                                knight.getFame(),
                                knight.getHealth()));
                    }
                    if (knight.getHealth() <= 0) {
                        died(knight);
                    } else if (knight.getFame() >= knight.myFairLady().fameExpectedFromKnight()) {
                        happyEnding(knight);
                    }
                }
            }

        }
    }

    private KnightIF getaKnight() {
        return context.getBean("knight", KnightIF.class);
    }

    private Quest getAQuest() {
        return (Quest) context.getBean("quest");
    }

    private List<KnightIF> getKnights(int nr) {
        List<KnightIF> ret = new ArrayList<>();
        for (int i = 0; i < nr; i++) {
            ret.add(getaKnight());
        }
        return ret;
    }

    private void died(KnightIF knight) {
        System.out.println(String.format("A nagy erejű, tekintélyes és nemes %s lovag rettenetes haláláról kell most megemlékeznünk."
                + "Meghallt a kiváló, a nagyszerű, az érdemekkel teljes.",
                knight.getName()));
        knight.myFairLady().myKnigths().remove(knight);
        knights.remove(knight);
    }

    private void happyEnding(KnightIF knight) {
        System.out.println(String.format("Az utlérhetetlen és nagyszerű %s lovag elérte leghőbb célját, csókot kapott úrhölgyétől: %s-től. "
                + "Történetükről nncs mit tovább zengenem.",
                knight.getName(),
                knight.myFairLady().getName()));
        List<Knight> myKnigths = knight.myFairLady().myKnigths();
        for (Knight k : myKnigths) {
            knights.remove(k);
        }
    }
}
