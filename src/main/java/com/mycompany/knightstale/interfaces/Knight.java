/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.knightstale.interfaces;

import com.mycompany.knightstale.enums.KnightActivity;
import java.util.Objects;
import java.util.UUID;
import javax.annotation.PostConstruct;

/**
 *
 * @author peti
 */
public abstract class Knight implements KnightIF{
    
    private HighLady myFairLady;
    private int health = 100;
    private int fame = 0;
    private String name = getClass().getSimpleName() + "_" +  UUID.randomUUID().toString();
    
    public Knight(HighLady myLady){
        myFairLady = myLady;
        myFairLady.addKnight(this);
    }

    public HighLady myFairLady() {
        return myFairLady;
    }

    public  int getHealth() {
        return health;
    }

    public  int getFame() {
        return fame;
    }

    public  void addHealth(int health) {
        this.health += health;
    }

    public  void addFame(int fame) {
        this.fame += fame;
    }

    public String getName() {
        return name;
    }

    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if(!(obj instanceof Knight)){
            return false;
        }
        final Knight other = (Knight) obj;
        return this.getName().equals(other.getName());
    }

    @Override
    public String toString() {
        return "Knight{" + "name=" + name + '}';
    }
    
    

    public abstract boolean takesQuest(Quest quest);

    public abstract KnightActivity actInQuest(Quest quest);

 
    
}
