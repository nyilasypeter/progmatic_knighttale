/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.knightstale.interfaces;

import com.mycompany.knightstale.enums.KnightActivity;
import java.util.Objects;

/**
 *
 * @author peti
 */
public interface KnightIF {
    
    public HighLady myFairLady();

    public  int getHealth();

    public  int getFame();

    public  void addHealth(int health);

    public  void addFame(int fame);

    public String getName();  

    public abstract boolean takesQuest(Quest quest);

    public abstract KnightActivity actInQuest(Quest quest);

 
    
}
