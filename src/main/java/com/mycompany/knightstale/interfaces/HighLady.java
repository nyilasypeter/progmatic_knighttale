/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.knightstale.interfaces;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author peti
 */
public abstract class HighLady {
    
    List<Knight> knights = new ArrayList<>();
    String name = getClass().getSimpleName() + "_" + UUID.randomUUID().toString();

    public List<Knight> myKnigths() {
        return knights;
    }


    public void addKnight(Knight k) {
        knights.add(k);
    }

    public String getName(){
        return name;
    }

    public abstract int fameExpectedFromKnight();
    
}
