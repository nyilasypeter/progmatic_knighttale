/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.knightstale.interfaces;

import com.mycompany.knightstale.data.QuestRisk;

/**
 *
 * @author peti
 */
public interface Quest {
    String getQuestName();
    String getQuestDescription();
    QuestRisk getRiskOfQuest();
    int getQuestFame();
    boolean doQuest(KnightIF knight);
}
