/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.knightstale.enums;

/**
 *
 * @author peti
 */
public enum KnightActivity {
    makeSpeechBeforeFight, strikeWithSword, defendWithShield, 
    attackUnexpectedly, runAway, writeALovePoem, 
    chantALoveSong, praiseMyLady;
}
