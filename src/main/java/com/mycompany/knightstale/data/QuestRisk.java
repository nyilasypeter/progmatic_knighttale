/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.knightstale.data;

import com.mycompany.knightstale.enums.Risk;

/**
 *
 * @author peti
 */
public class QuestRisk {
    Risk probabilityOfDeath;
    Risk probabililtyOfInjury;

    public QuestRisk(Risk probabilityOfDeath, Risk probabililtyOfInjury) {
        this.probabilityOfDeath = probabilityOfDeath;
        this.probabililtyOfInjury = probabililtyOfInjury;
    }

    public Risk getProbabilityOfDeath() {
        return probabilityOfDeath;
    }

    public Risk getProbabililtyOfInjury() {
        return probabililtyOfInjury;
    }

    
}
