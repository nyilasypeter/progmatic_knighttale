package com.mycompany.knightstale.spring;

import com.mycompany.knightstale.implementations.Bicskanyitogato_Jakab;
import com.mycompany.knightstale.implementations.Caius_a_Valogatos;
import com.mycompany.knightstale.implementations.DonQuijote;
import com.mycompany.knightstale.implementations.Finn_Thor;
import com.mycompany.knightstale.implementations.Gregorius;
import com.mycompany.knightstale.implementations.Horkolo_Orkok;
import com.mycompany.knightstale.implementations.LorenzoVonMatterhorn;
import com.mycompany.knightstale.implementations.Norin_az_Ovatos;
import com.mycompany.knightstale.implementations.Sir_Christian;
import com.mycompany.knightstale.implementations.Tank_Aranka;
import com.mycompany.knightstale.implementations.TheRussianMagician;
import com.mycompany.knightstale.interfaces.HighLady;
import com.mycompany.knightstale.interfaces.Knight;
import com.mycompany.knightstale.interfaces.KnightIF;
import com.mycompany.knightstale.interfaces.Quest;
import java.beans.Introspector;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Scope;

@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackageClasses = {Bicskanyitogato_Jakab.class})
public class Springconfig {
    
    private static final List<String> knightNames = new ArrayList<>();
    static{
        knightNames.add(getBeanNameFromClass(Bicskanyitogato_Jakab.class));
        knightNames.add(getBeanNameFromClass(Caius_a_Valogatos.class));
        knightNames.add(getBeanNameFromClass(DonQuijote.class));
        knightNames.add(getBeanNameFromClass(Finn_Thor.class));
        knightNames.add(getBeanNameFromClass(Gregorius.class));
        knightNames.add(getBeanNameFromClass(LorenzoVonMatterhorn.class));
        knightNames.add(getBeanNameFromClass(Norin_az_Ovatos.class));
        knightNames.add(getBeanNameFromClass(Sir_Christian.class));
    }
    
    Random r = new Random();

    private static String getBeanNameFromClass(Class<? extends Knight> aClass) {
        String simpleName = aClass.getSimpleName();
        return Introspector.decapitalize(simpleName);
    }
    
    @Autowired
    ApplicationContext context;
    

    @Bean
    @Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
    public KnightIF knight() {
        return context.getBean(knightNames.get(r.nextInt(knightNames.size())), KnightIF.class);
    }
    
    @Bean
    @Qualifier("randomLady")
    public HighLady highlady() {
        return new Tank_Aranka();
    }
    
    @Bean
    @Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
    public Quest quest() {
        return new TheRussianMagician();
    }

}
